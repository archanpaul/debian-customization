## Debian post-install script
#!/bin/bash

APTGET="apt-get install --no-install-recommends --assume-yes"
DEBIAN_REPOS="/home/public/debian-repos/"

## Setup apt

function setup_apt_stable() {
	cp /etc/apt/sources.list /etc/apt/sources.list.stable
	
	echo "deb http://ftp.us.debian.org/debian stable main" > /etc/apt/sources.list
	apt-get update
	$APTGET apt-cacher-ng links
	echo "Acquire::http::Proxy \"http://localhost:3142/\";" > /etc/apt/apt.conf.d/000apt-cacher-ng-proxy
	/etc/init.d/apt-cacher-ng restart
	mv /var/cache/apt-cacher-ng $DEBIAN_REPOS/
	ln -s $DEBIAN_REPOS/apt-cacher-ng/ /var/cache/apt-cacher-ng
	mkdir $DEBIAN_REPOS/apt-cacher-ng/_import
	echo "copy all importable packages into $DEBIAN_REPOS/apt-cacher-ng/_import"
	echo "links http://localhost:3142 import packages"
	#links http://localhost:3142 
	#apt-get update
	#apt-get dist-upgrade
}

function setup_apt_sid() {
	cp /etc/apt/sources.list /etc/apt/sources.list.previous
	
	echo "deb http://ftp.us.debian.org/debian sid main contrib non-free" > /etc/apt/sources.list
	echo "Acquire::http::Proxy \"http://localhost:3142/\";" > /etc/apt/apt.conf.d/000apt-cacher-ng-proxy
	##deb_multimedia
	apt-key advanced --keyserver pgp.mit.edu --recv-keys 1F41B907
	apt-key advanced --keyserver pgp.mit.edu --recv-keys 65558117
	echo "deb http://www.deb-multimedia.org sid main non-free" >> /etc/apt/sources.list
	##google_linux_softwares
	wget -q -O - http://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
	echo "deb http://dl.google.com/linux/talkplugin/deb/ stable main" >> /etc/apt/sources.list
	echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >>  /etc/apt/sources.list
	echo "deb http://dl.google.com/linux/earth/deb/ stable main" >> /etc/apt/sources.list
	#echo "deb [arch=amd64] https://storage.googleapis.com/download.dartlang.org/linux/debian stable main" >> /etc/apt/sources.list
	apt-get update
	##import packages
	echo "copy all importable packages into $DEBIAN_REPOS/apt-cacher-ng/_import"
	echo "links http://localhost:3142 import packages"
	#links http://localhost:3142 
	#apt-get update
	#apt-get dist-upgrade
}

## Base
function base() {
	$APTGET grub-pc
	$APTGET systemd-sysv
	$APTGET dbus rsyslog cron anacron
	$APTGET linux-image-amd64 
	$APTGET firmware-linux-free firmware-linux-nonfree
	$APTGET firmware-brcm80211
        apt-get install --assume-yes acpi-call-dkms
	echo "acpi_call" > /etc/modules-load.d/acpi_call.conf
	echo "w /proc/acpi/call - - - - \_SB.PCI0.P0P2.PEGP._OFF" > /etc/tmpfiles.d/acpi_call.conf
	update-initramfs -u
}

## Console tools
function console_tools() {
	$APTGET aptitude apt-transport-https
	$APTGET gdisk e2fsprogs ecryptfs-utils lvm2 mtpfs 
	$APTGET acpi acpid pm-utils powertop
	$APTGET hdparm smartmontools
	$APTGET bc sudo mc less
	$APTGET links
	$APTGET ntpdate dnsutils
	$APTGET minicom
	$APTGET dvd+rw-tools 
	$APTGET curl 
	$APTGET	zip unzip p7zip p7zip-full bzip2 unrar-free
	$APTGET arj lhasa lzip lzop ncompress rpm2cpio rzip sharutils unace unalz zoo
	$APTGET gnupg
	$APTGET laptop-mode-tools 
}

## Network tools
function network_tools() {
	$APTGET nmap ufw
	$APTGET whois udns-utils
	$APTGET lftp rsync wget 
	$APTGET usb-modeswitch wvdial
	$APTGET net-tools ethtool bridge-utils
	$APTGET rfkill wireless-tools wpasupplicant
	$APTGET bluez bluez-tools
	$APTGET network-manager network-manager-openconnect network-manager-openvpn network-manager-pptp network-manager-vpnc
	# zeroconf
	$APTGET avahi-daemon avahi-discover libnss-mdns
}

## Xorg
function xorg() {
	$APTGET xserver-xorg-core 
	$APTGET xserver-xorg-video-intel xserver-xorg-video-ati 
	$APTGET xserver-xorg-input-kbd xserver-xorg-input-mouse xserver-xorg-input-synaptics xserver-xorg-input-evdev
	$APTGET xinit xdm xnest
	$APTGET desktop-base
}

## Xorg Dev
function xorg-dev () {
	$APTGET libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev
}

## SCM
function scm() {
	$APTGET git-core git-remote-bzr git-remote-hg git-svn
}

## Printing
function printing() {
	$APTGET cups foomatic-db-compressed-ppds hplip 
	$APTGET sane 
}

## Dev tools
function dev_tools() {
	$APTGET build-essential gcc-multilib g++-multilib
	$APTGET autoconf automake make cmake scons libtool
	$APTGET patch pkg-config
	$APTGET flex bison gperf 
	$APTGET gdb valgrind
	$APTGET doxygen
	$APTGET libc6-dev zlib1g-dev 
	$APTGET libboost-dev libopencv-dev
}

## Android dev-tools
function android_dev_tools() {
	#$APTGET android-tools-adb android-tools-fastboot android-tools-fsutils
	#APTGET u-boot-tools
	$APTGET lib32z1-dev lib32ncurses5-dev lib32readline-gplv2-dev
}

## Java dev-tools
function java_dev_tools() {
	$APTGET openjdk-8-jre
	#$APTGET icedtea-7-plugin
	$APTGET openjdk-8-jdk
	$APTGET java-package
}

## Python dev-tools
function python_dev_tools() {
	$APTGET python3 python3-doc python3-dev
	$APTGET python3-pip python3-setuptools python3-virtualenv virtualenv
	$APTGET pylint pychecker python-flake8 python3-flake8 python3-pep8 python3-jedi python3-mccabe
	$APTGET python3-django python3-pil python3-docutils python3-crypto
	$APTGET python3-psycopg2 python3-pymongo
}

## Multimedia
function multimedia() {
	$APTGET gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly 
	$APTGET gstreamer0.10-plugins-base gstreamer0.10-plugins-good gstreamer0.10-plugins-bad gstreamer0.10-plugins-ugly 
	$APTGET pulseaudio 
	$APTGET libwebrtc-audio-processing-0
	$APTGET lame libid3-tools flac
	$APTGET vlc 
}

## Ebooks tools
function ebook_tools() {
	$APTGET calibre 
}

## Office suite
function office_suite() {
	$APTGET libreoffice-writer libreoffice-calc libreoffice-draw libreoffice-impress libreoffice-math libreoffice-base
	$APTGET libreoffice-style-sifr
	$APTGET libreoffice-lightproof-en
	$APTGET hunspell-en-us
	$APTGET simple-scan
}

## Editors
function editors() {
	$APTGET aspell-en
	$APTGET vim
	$APTGET emacs
	$APTGET pymacs python-mode
}

## Graphic utils
function graphic_utils() {
	$APTGET gimp
	$APTGET inkscape
}

## Databases
function databases() {
	$APTGET sqlite3 
	$APTGET mariadb-server mariadb-client
	$APTGET postgresql postgresql-client
	$APTGET postgresql-server-dev-all
	$APTGET mongodb-server mongodb-clients
}

## Servers
function servers() {
	$APTGET openssh-server openssh-client
	$APTGET openvpn openvpn-auth-ldap openvpn-blacklist pptpd
	$APTGET apache2
	$APTGET libapache2-mod-wsgi-py3
	$APTGET nodejs
	$APTGET slapd ldap-utils
}

## Documentations 
function documentations() {
	$APTGET manpages manpages-dev
	$APTGET glibc-doc
}

## Browsers
function browsers() {
	$APTGET chromium
	$APTGET iceweasel iceweasel-adblock-plus iceweasel-pdf.js iceweasel-scrapbook iceweasel-firebug
	$APTGET icedove icedove-gcontactsync iceowl-extension calendar-google-provider
	$APTGET flashplugin-nonfree
}

## Fonts
function fonts() {
	$APTGET ttf-liberation 
	#$APTGET ttf-indic-fonts
	#$APTGET ttf-hanazono
}

## Localized input-systems
function localized_input_systems() {
	$APTGET fcitx fcitx-m17n fcitx-mozc
	$APTGET fcitx-ui-light
}

## Virtualization
function virtualization() {
	$APTGET qemu-system qemu-kvm
	$APTGET libvirt -bin 
	$APTGET linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,') virtualbox virtualbox-qt virtualbox-dkms
}

## Generic Themes
function generic_themes() {
	$APTGET plymouth
	$APTGET xcursor-themes
	$APTGET gtk2-engines-oxygen gtk2-engines-pixbuf gtk2-engines-murrine
	$APTGET gtk3-engines-oxygen gtk3-engines-unico
}

## Cinnamon desktop
function cinnamon_desktop() {
	$APTGET task-cinnamon-desktop
	$APTGET nemo-fileroller
	$APTGET lightdm lightdm-gtk-greeter
}

## GNOME desktop
function gnome_desktop() {
	$APTGET task-gnome-desktop
	#apt-get install task-gnome-desktop
	$APTGET gnome-tweak-tool
	$APTGET gnome-power-manager
	$APTGET network-manager-gnome
	$APTGET gnome-shell-extension-autohidetopbar
	$APTGET libreoffice-gnome
	$APTGET pavucontrol
	$APTGET rhythmbox rhythmbox-plugins rhythmbox-plugin-cdrecorder sound-juicer
	$APTGET gedit gedit-plugins
	$APTGET shotwell cheese
	$APTGET vinagre
	$APTGET transmission-gtk
	$APTGET file-roller
}

## KDE desktop
function kde_desktop() {
	$APTGET desktop-base kde-plasma-desktop
	$APTGET kdeadmin kdegraphics kdemultimedia kdepim kdenetwork kdeutils 
	$APTGET kdm
	$APTGET kde-style-oxygen kde-config-gtk-style kde-config-touchpad
	$APTGET kde-telepathy kde-config-telepathy-accounts telepathy-gabble telepathy-haze telepathy-idle telepathy-rakia telepathy-ring telepathy-salut
	$APTGET plasma-widgets-workspace
	$APTGET plasma-widget-networkmanagement plasma-widget-kimpanel
	$APTGET kate okular okular-extra-backends
	$APTGET akonadi-backend-sqlite
	$APTGET bluedevil
	$PATGET kde-config-touchpad kde-config-fcitx
	$APTGET phonon phonon-backend-gstreamer phonon-backend-vlc
	$APTGET ktorrent 
	$APTGET skanlite
	$APTGET k3b amarok 
	$APTGET digikam
	$APTGET libreoffice-kde libreoffice-style-oxygen
	#APTGET calligra
	$APTGET kmplot kalgebra
}

## QT dev
function qt_dev() {
	$APTGET qt5-default qtbase5-dev qtbase5-dev-tools qtcreator
	$APTGET libqt5webkit5-dev libqt5opengl5-dev libqt5svg5-dev libqt5sensors5-dev 
	$APTGET libqjson-dev
	$APTGET qgit
	$APTGET sqlitebrowser
}

## Android dev
function android_dev() {
	$APTGET lib32z1
}

## Google Linux softwares
function google_linux_softwares() {
	#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
	#echo "deb http://dl.google.com/linux/talkplugin/deb/ stable main" >> /etc/apt/sources.list
	#echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >>  /etc/apt/sources.list
	#echo "deb http://dl.google.com/linux/earth/deb/ stable main" >> /etc/apt/sources.list
	#echo "deb [arch=amd64] https://storage.googleapis.com/download.dartlang.org/linux/debian stable main" >> /etc/apt/sources.list
	apt-get update
	$APTGET google-talkplugin
	rm -rf /etc/apt/sources.list.d/google-talkplugin.list
	#$APTGET google-chrome-stable
	rm -rf /etc/apt/sources.list.d/google-chrome.list
	#$APTGET google-earth-stable
	#rm -rf /etc/apt/sources.list.d/google-earth.list
	#$APTGET dart
}

## Deb Multimedia
function deb_multimedia() {
	#apt-key advanced --keyserver pgp.mit.edu --recv-keys 1F41B907
	#apt-key advanced --keyserver pgp.mit.edu --recv-keys 65558117
	#echo "deb http://www.deb-multimedia.org sid main non-free" >> /etc/apt/sources.list
	#apt-get update
	$APTGET faac
	$APTGET ffmpeg gstreamer0.10-ffmpeg gstreamer1.0-libav
}

## systemd services
function systemd_services() {
	systemctl disable apache2.service
	systemctl disable mysql.service
	systemctl disable slapd.service
	systemctl disable avahi-daemon.service
	systemctl disable bluetooth.service
	systemctl disable cups.service
	systemctl disable mongodb.service
	systemctl disable openvpn.service
	systemctl disable postgresql.service
	systemctl disable pptpd.service
	systemctl disable pppd-dns.service
	systemctl disable rsync.service
	systemctl disable rsyslog.service
	systemctl disable smartd.service
	systemctl disable smartmontools.service
	systemctl disable ssh.service
	systemctl disable sshd.service

	systemctl enable acpid.service
	systemctl enable anacron.service
	systemctl enable apt-cacher-ng.service
	systemctl enable cron.service
	systemctl enable gdm.service
	systemctl enable laptop-mode.service
	systemctl enable lvm2.service
	systemctl enable network-manager.service
	systemctl enable NetworkManager.service
	systemctl enable syslog.service
	systemctl enable upower.service
	systemctl enable wpa_supplicant.service
	systemctl enable ufw.service
}

function install_all_modules() {
	base
	console_tools
	xorg
	scm
	printing
	dev_tools
	android_dev_tools
	java_dev_tools
	python_dev_tools
	multimedia
	ebook_tools
	office_suite
	editors
	graphic_utils
	databases
	servers
	documentations
	browsers
	fonts
	localized_input_systems
	virtualization
	generic_themes
	android_dev
	systemd_services
	google_linux_softwares
	deb_multimedia
	gnome_desktop
	#cinnamon_desktop
	network_tools
}

#setup_apt_stable
#setup_apt_sid
#apt-get update
#apt-get dist-upgrade

install_all_modules 2>&1 | tee debian-postinstall.log
echo `date` >> debian-postinstall.log

